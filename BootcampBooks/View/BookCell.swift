//
//  BookCell.swift
//  BootcampBooks
//
//  Created by Keith Coughtrey on 26/04/17.
//  Copyright © 2017 NativeMobile. All rights reserved.
//

import UIKit
import SDWebImage

class BookCell: UITableViewCell {

    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var pageCountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(book: Book) {
        if let url = book.thumbnailPath {
            thumbnail.sd_setImage(with: URL(string: url.replacingOccurrences(of: "http", with: "https")))
        }
        titleLabel.text = book.title ?? ""
        descriptionLabel.text = book.description ?? ""
        pageCountLabel.text = String(book.pageCount ?? 0)
    }
}
