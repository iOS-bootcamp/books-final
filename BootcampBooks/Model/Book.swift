//
//  Book.swift
//  BootcampBooks
//
//  Created by Keith Coughtrey on 26/04/17.
//  Copyright © 2017 NativeMobile. All rights reserved.
//

import Foundation
import ObjectMapper

class Book : Mappable {
    var title: String?
    var description: String?
    var thumbnailPath: String?
    var pageCount: Int?
    var publishedOn: String?

    var publishedYear: String? {
        if let published = publishedOn, published.characters.count > 3 {
            let index = published.index(published.startIndex, offsetBy: 4)
            return published.substring(to: index)
        }
        return nil
    }

    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        title <- map["volumeInfo.title"]
        description <- map["volumeInfo.description"]
        thumbnailPath <- map["volumeInfo.imageLinks.smallThumbnail"]
        pageCount <- map["volumeInfo.pageCount"]
        publishedOn <- map["volumeInfo.publishedDate"]
    }

}
