//
//  BookRepository.swift
//  BootcampBooks
//
//  Created by Keith Coughtrey on 26/04/17.
//  Copyright © 2017 NativeMobile. All rights reserved.
//

import Foundation
import RxSwift

protocol BookRepository {
    func list(query: String) -> Observable<BookList>
}

class BookRepositoryImpl: BookRepository {
    
    static let shared: BookRepository = BookRepositoryImpl()
    
    func list(query: String) -> Observable<BookList> {
        return RequestService.call(url: "https://www.googleapis.com/books/v1/volumes?q=\(query)&maxResults=40")
    }
}


