//
//  ArrayExtension.swift
//  BootcampBooks
//
//  Created by Keith Coughtrey on 26/04/17.
//  Copyright © 2017 NativeMobile. All rights reserved.
//

import Foundation

extension Array where Element : Hashable {
    var distinct: [Element] {
        return Array(Set(self))
    }
}
